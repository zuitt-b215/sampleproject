<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/hello', function () {
//     return view('hello');
// });

// ACTIVITY
Route::get('/hello', [PageController::class,'hello'] );
Route::get('/about', [PageController::class,'about'] );
Route::get('/services', [PageController::class,'services'] );
Route::get('/', function () {
    return view('pages.index');
});

Route::resource('/posts', PostController::class);



Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
