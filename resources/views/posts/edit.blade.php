@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <h1>Edit Post</h1>
    <form method="post" action="/posts/{{$post->id}}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" placeholder="title" name="title" type="text" id="title" value="{{ $post->title }}">
        </div>

        <div class="form-group">
            <label for="title">Body</label>
            <textarea class="form-control" placeholder="body" name="body" rows="10" id="body">{{ $post->body }}</textarea>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Update</button>
    </form>

<script>
    ClassicEditor
    .create( document.querySelector( '#body' ) )
    .catch( error => {
    console.error( error );
    } );
</script>
@endsection