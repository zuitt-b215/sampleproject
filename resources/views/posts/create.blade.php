@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

	<h1>Create Post</h1>
	<form method="post" action="/posts">
		@csrf
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" class="form-control" name="title">
		</div>
		<div class="form-group">
                <label for="title">Body</label>
                <textarea class="form-control" name="body" rows="10" id="body"></textarea>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Submit</button>
	</form>

<script>
	ClassicEditor
	.create( document.querySelector( '#body' ) )
	.catch( error => {
	console.error( error );
	} );
</script>
@endsection