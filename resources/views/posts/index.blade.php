@extends('layouts.app')

@section('content')
	<h1>Posts</h1>

	@if(count($posts) > 0)
		@foreach($posts as $post)
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">
						<a href="/posts/{{ $post->id }}">{{ $post->title }}</a>
					</h5>
					<small>Written on: {{ $post->created_at }}</small>
				</div>
			</div>
			<br>
		@endforeach
		{{ $posts->links() }}
	@endif
@endsection