@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                    <hr>
                    <a type="button" class="btn btn-primary" href="/posts/create">Create Post</a>

                    <h2>My Posts</h2>
                    @if(count($posts) > 0)
                        <table class="table table-header">
                            <tr>
                                <th>Title</th>
                                <th></th>
                                <th></th>
                            </tr>
                        @foreach($posts as $post)
                            <tr>
                                <td>
                                    {{ $post->title }}
                                </td>
                                <td>
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>
                                </td>
                                <td>
                                    <form method="post" action="/posts/{{$post->id}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
